import React from 'react';
import { Routes, Route } from 'react-router-dom';

import Footer from './admin/adminfooter';
import Navbar from './admin/adminheader';
import TransactionTable from './admin/transactionTable';
import ReservationTable from './admin/reservationTable';
import Help from './admin/help';
import AboutUs from './admin/aboutus';
import RulesAndRegulations from './admin/rulesandregulation';
import PrivacyPolicy from './admin/privacypolicy';
import SiteMap from './admin/sitemap';
import Tickets from './admin/ticket';
import ContentUpload from './admin/content';
import Attractions from './admin/attraction';

function Admin() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path="/home" element={<ReservationTable />} />
        <Route path="/transactions" element={<TransactionTable />} />
        <Route path="/help" element={<Help />} />
        <Route path="/aboutus" element={<AboutUs />} />
        <Route path="/tickets" element={<Tickets />} />
        <Route path="/rulesandregulations" element={<RulesAndRegulations />} />
        <Route path="/privacypolicy" element={<PrivacyPolicy />} />
        <Route path="/sitemap" element={<SiteMap />} />
        <Route path="/content" element={<ContentUpload />} />
        <Route path="/attraction" element={<Attractions />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default Admin;