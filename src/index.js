import React from 'react';
import { createRoot } from 'react-dom/client'; 
import './index.css';
import App from './App'; 
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const container = document.getElementById('root'); // Ensure this matches the div id in your HTML

if (container) {
  const root = createRoot(container); // Create the root using createRoot
  root.render(
    <React.StrictMode>
      <App />
      <ToastContainer
        position='top-right'
        autoClose={500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
        bodyClassName="toastBody"
      />
    </React.StrictMode>
  );
} else {
  console.error('Failed to find the root element.');
}

reportWebVitals();
