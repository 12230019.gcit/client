import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, Navigate, useLocation } from 'react-router-dom';
import axios from 'axios';
import Signup from './pages/Signup';
import Login from './pages/login';
import AdminLayout from './admin';
import UserLayout from './User';
import GuestLayout from './GuestUser';
import ForgotPassword from './pages/forgotPassword';

const App = () => {
  const token = localStorage.getItem('token');
  const [isLoggedIn, setIsLoggedIn] = useState(!!token);
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const checkAuthStatus = async () => {
      try {
        const token = localStorage.getItem('token');
        if (token) {
          const response = await axios.get('http://localhost:8080/api/user/verify-token', {
            headers: { Authorization: `Bearer ${token}` },
          });

          if (response.data.status === 'success') {
            setIsLoggedIn(true);
            setIsAdmin(response.data.data.user.isAdmin);
          }
        }
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    checkAuthStatus();
  }, []);

  const handleLogin = (isLoggedIn, isAdmin) => {
    setIsLoggedIn(isLoggedIn);
    setIsAdmin(isAdmin);
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <BrowserRouter>
      <AppRoutes isLoggedIn={isLoggedIn} isAdmin={isAdmin} handleLogin={handleLogin} />
    </BrowserRouter>
  );
};

const AppRoutes = ({ isLoggedIn, isAdmin, handleLogin }) => {
  const location = useLocation();

  if (isLoggedIn) {
    if (isAdmin) {
      return (
        <Routes>
          <Route path='/admin/*' element={<AdminLayout />} />
          <Route path='*' element={<Navigate to='/admin' replace />} />
        </Routes>
      );
    } else {
      return (
        <Routes>
          <Route path='/*' element={<UserLayout />} />
          <Route path='*' element={<Navigate to='/' replace />} />
        </Routes>
      );
    }
  } else {
    return (
      <Routes>
        <Route path='/login' element={<Login onLogin={handleLogin} />} />
        <Route path='/signup' element={<Signup />} />
        <Route path='/forgotpassword' element={<ForgotPassword />} />
        <Route path='/guest/*' element={<GuestLayout />} />
        <Route path='*' element={<Navigate to='/guest' replace />} />
      </Routes>
    );
  }
};

export default App;


