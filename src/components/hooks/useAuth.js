// import { useState, useEffect } from 'react';

// function useAuth() {
//     const [isAuthenticated, setIsAuthenticated] = useState(false);
//     const [user, setUser] = useState(null);
//     const [authorizationToken, setAuthorizationToken] = useState(null);

//     useEffect(() => {
//         const token = localStorage.getItem('authToken');
//         if (token) {
//             setIsAuthenticated(true);
//             setAuthorizationToken(token);
//             fetchUserDetails(token);
//         } else {
//             console.log("User is not authenticated");
//         }
//     }, []);

//     const fetchUserDetails = async (token) => {
//         try {
//             const response = await fetch('http://localhost:8080/api/user/', {
//                 headers: {
//                     Authorization: `Bearer ${token}`
//                 }
//             });
//             if (response.ok) {
//                 const data = await response.json();
//                 setUser(data);
//             } else {
//                 console.log("Failed to fetch user details");
//             }
//         } catch (error) {
//             console.error("An error occurred while fetching user details", error);
//         }
//     };

//     const login = (token) => {
//         localStorage.setItem('authToken', token);
//         setAuthorizationToken(token);
//         setIsAuthenticated(true);
//         fetchUserDetails(token);
//     };

//     const logout = () => {
//         localStorage.removeItem('authToken');
//         setIsAuthenticated(false);
//         setUser(null);
//         setAuthorizationToken(null);
//     };

//     return { isAuthenticated, user, authorizationToken, login, logout };
// }

// export default useAuth;
import { useState, useEffect } from 'react';

function useAuth() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);

    useEffect(() => {
        const jwt = localStorage.getItem('jwt');
        const adminStatus = localStorage.getItem('isAdmin') === 'true';
        if (jwt) {
            setIsAuthenticated(true);
            setIsAdmin(adminStatus);
        }
    }, []);

    return { isAuthenticated, isAdmin };
}

export default useAuth;
