import { useState } from 'react';
import React from "react";
import './footer.css'; // Import your CSS file for styling
import { FaFacebook, FaTwitter, FaInstagram, FaYoutube, FaTiktok } from 'react-icons/fa'; // Import social media icons
import { Link } from "react-router-dom";

const Footer = () => {
    const token = localStorage.getItem('token');
    const [isLoggedIn] = useState(!!token);
    return (
        <div>
            <footer className="footer py-5 mt-5">
                <div className="container">
                    <div className="row footer-container justify-content-center">
                        <div className="col-12 col-md-2 mt-3">
                            <ul className="footer-list">
                                <li className="footer__content-item">
                                    <Link to="/aboutus">Our Services</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to="/attraction">Attractions</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to={isLoggedIn ? '/tickets' : '/signup'}>
                                        Tickets
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-12 col-md-2 mt-3">
                            <ul className="footer-list">
                                <li className="footer__content-item">
                                    <Link to="/privacypolicy">Privacy Policy</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to="/rules">Rules and Regulations</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-12 col-md-2 text-center mt-3">
                            <Link to="/home">
                                <img src="./Assets/logoWhite.png" alt="Your Logo" className="footer-logo mb-3" />
                            </Link>
                            <h3 className="footer-heading" style={{ color: "white", fontWeight: "bold" }}>Aqua Park</h3>
                            <div className="social-icons mt-3">
                                <FaFacebook className="social-icon" />
                                <FaTwitter className="social-icon" />
                                <FaYoutube className="social-icon" />
                                <FaTiktok className="social-icon" />
                                <FaInstagram className="social-icon" />
                            </div>
                        </div>
                        <div className="col-12 col-md-2 mt-3">
                            <ul className="footer-list">
                                <li className="footer__content-item">
                                    <Link to="/aboutus">About Us</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to="/sitemap">Sitemap</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-12 col-md-2 mt-3">
                            <ul className="footer-list">
                                <li className="footer__content-item">
                                    <Link to="/help">Contact Us</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to="/help">FAQs</Link>
                                </li>
                                <li className="footer__content-item">
                                    <Link to="/help">Location</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <div>
                <div className="col-12 text-center mt-2">
                    <p className="footer__copyright">
                        © {new Date().getFullYear()} All rights reserved
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Footer;
