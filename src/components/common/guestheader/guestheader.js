import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import "../header/Navbar.css";
import 'bootstrap/dist/css/bootstrap.min.css';


const Navbar = () => {


    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };


    return (
        <>
            <nav className="navbar navbar-expand-sm sticky-top">
                <div className="container-fluid">
                    <div className="nav-logo justify-content-center">
                        <NavLink to="/"><img src="./Assets/logoWhite.png" className="nav_logo" alt="logo" /></NavLink>
                    </div>
                    <button className="navbar-toggler navbar-dark" type="button" onClick={toggleMenu}>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className={`collapse navbar-collapse justify-content-end  ${isMenuOpen ? 'show' : ''} `} >
                        <ul className=" navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/guest/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/guest/aboutus">AboutUs</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/guest/attraction">Attractions</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/signup">Tickets</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/guest/help">Help</Link>
                            </li>
                            <li className="nav-item">
                                <Link className='nav-link' to="/login" style={{ fontWeight: "bold" }}>LOGIN</Link>
                            </li>
                            <li className="nav-item">
                                <Link className='nav-link' to="/signup" style={{ fontWeight: "bold" }}>SIGNUP</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}

export default Navbar;
