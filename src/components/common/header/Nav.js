import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import "./Navbar.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';

const Navbar = () => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const [showOverlay, setShowOverlay] = useState(false);

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };
    const handleLogoutClick = () => {
        setShowOverlay(true);
    };
    const handleClose = () => {
        setShowOverlay(false);
    };

    function logout() {
        fetch('http://localhost:8080/api/user/logout')
            .then(response => {
                if (response.ok) {
                    localStorage.removeItem('token');
                    localStorage.removeItem('isAdmin');
                    window.open("./", "_self")
                } else {
                    throw new Error(response.statusText)
                }
            }).catch(e => {
                alert(e)
            })
    }

    return (
        <>
            <nav className="navbar navbar-expand-sm sticky-top scrol">
                <div className="container-fluid">
                    <div className="nav_logo justify-content-center">
                        <Link to="/home"><img src="./Assets/logoWhite.png" className="nav_logo" alt="logo" /></Link>
                    </div>
                    <button className="navbar-toggler navbar-dark" type="button" onClick={toggleMenu}>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className={`collapse navbar-collapse ms-5 justify-content-end ${isMenuOpen ? 'show' : ''} `} >
                        <ul className=" navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/home" activeClassName="active">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/aboutus" activeClassName="active">AboutUs</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/attraction" activeClassName="active">Attractions</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/tickets" activeClassName="active">Tickets</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/help" activeClassName="active">Help</Link>
                            </li>
                            <li className="nav-item">
                                <Link className='nav-link ' onClick={handleLogoutClick} >LOG OUT </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <Modal className="center-modal" show={showOverlay} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>Are you sure you want to log out?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    You will be signed out of your account and any unsaved changes may be lost.
                </Modal.Body>
                <Modal.Footer className='d-flex row'>
                    <button className='logout-custom-btn' onClick={() => setShowOverlay(false)}>
                        Cancel
                    </button>
                    <button className="logout-button" onClick={() => (logout())} >
                        Log Out
                    </button>
                </Modal.Footer>
            </Modal>

        </>
    );
}

export default Navbar;
