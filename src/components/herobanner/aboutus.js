import React from 'react';
import { useState } from 'react';
import './herobanner.css';
import backgroundVideo from '../herobanner/aboutus.mp4'; // Ensure the correct relative path to your video file
import Bubbles from './Bubbles';
import { Link } from 'react-router-dom';

const HeroBanner = () => {
    const token = localStorage.getItem('token');
    const [isLoggedIn] = useState(!!token);
    return (
        <>
            <div>
                <Bubbles />
            </div>
            <div className="hero-container ">
                <video className="background-video img-fluid" autoPlay loop muted>
                    <source src={backgroundVideo} type="video/mp4" />
                </video>
                <div className="hero-content">
                    <h1 className="custom-font">Unveiling </h1>
                    <h1 className="custom-font">Our Journey</h1>
                    <p> “A Tale of Passion, Perseverance, and Purpose!”</p>
                    <Link to={isLoggedIn ? '/tickets' : '/signup'}>
                        <button

                            className="button"
                        >
                            BOOK NOW
                        </button>
                    </Link>
                </div>
            </div>
        </>
    );
};

export default HeroBanner;
