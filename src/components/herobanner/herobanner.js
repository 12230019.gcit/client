import React from 'react';
import { useState } from 'react';
import './herobanner.css';
import backgroundVideo from '../herobanner/vidd.mp4'; // Ensure the correct relative path to your video file
import Bubbles from './Bubbles';
import { Link } from 'react-router-dom';

const HeroBanner = () => {
    const token = localStorage.getItem('token');
    const [isLoggedIn] = useState(!!token);
    return (
        <>
            <div>
                <Bubbles />
            </div>
            <div className="hero-container ">
                <video className="background-video img-fluid" autoPlay loop muted>
                    <source src={backgroundVideo} type="video/mp4" />
                </video>
                <div className="hero-content">
                    <h1 className="custom-font">Welcome To</h1>
                    <h1 className="custom-font">Yonder Dive</h1>
                    <p>"Dive into Fun: Where Every Splash Tells A Story!"</p>
                    <Link to={isLoggedIn ? '/tickets' : '/signup'}>
                        <button

                            className="button"
                        >
                            BOOK NOW
                        </button>
                    </Link>
                </div>
            </div>
        </>
    );
};

export default HeroBanner;
