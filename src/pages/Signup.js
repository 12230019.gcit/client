import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import '../css/Signup.css';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { toast } from "react-toastify";
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';

const URL = "https://yodi-backend-1.onrender.com/api/user/register";

const Signup = () => {
    const [showOTPModal, setShowOTPModal] = useState(false);
    const [loading, setLoading] = useState(false);
    const [formData, setFormData] = useState({
        email: "",
        password: "",
        confirmPassword: "",
        firstName: "",
        lastName: "",
        showPassword: false,
        showConfirmPassword: false,
        termsAndConditions: false
    });
    const [otp, setOtp] = useState(new Array(6).fill(""));
    const [formErrors, setFormErrors] = useState({});
    const navigate = useNavigate();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleTogglePasswordVisibility = () => {
        setFormData({
            ...formData,
            showPassword: !formData.showPassword
        });
    };

    const handleToggleConfirmPasswordVisibility = () => {
        setFormData({
            ...formData,
            showConfirmPassword: !formData.showConfirmPassword
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        // Clear previous errors
        setFormErrors({});

        // Validation checks
        const errors = {};

        // Email validation
        if (formData.email === "") {
            errors.email = "Email is required";
        } else if (!formData.email.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
            errors.email = "Enter a valid email address";
        }

        // Password validation
        if (formData.password === "") {
            errors.password = "Password is required";
        } else if (!formData.password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/)) {
            errors.password = "Password must be at least 8 characters long and contain at least one digit, one lowercase letter, and one uppercase letter";
        }

        // Confirm password validation
        if (formData.confirmPassword === "") {
            errors.confirmPassword = "Confirm password is required";
        } else if (formData.password !== formData.confirmPassword) {
            errors.confirmPassword = "Passwords do not match";
        }

        // First name validation
        if (!formData.firstName.trim()) {
            errors.firstName = "First name is required";
        }

        // Last name validation
        if (!formData.lastName.trim()) {
            errors.lastName = "Last name is required";
        }

        if (!formData.termsAndConditions) {
            errors.termsAndConditions = "Please check the terms and conditions to create an account";
        }

        // Setting errors, if any
        if (Object.keys(errors).length > 0) {
            setFormErrors(errors);
            return;
        }

        try {
            const response = await axios.post(URL, formData);
            console.log(response)
            console.log("Signup successful.Check your email for the OTP.", response); // Add this line
            toast.success("Signup successful");
            setShowOTPModal(true);

        } catch (error) {
            if (error.response && error.response.data && error.response.data.message) {
                setFormErrors({ server: error.response.data.message });
                toast.error(error.response.data.message);
            } else {
                toast.error("An error occurred while signing up. Please try again later.");
            }
        } finally {
            setLoading(false)
        }

    };

    const handleOTPChange = (element, index) => {
        if (isNaN(element.value)) return false;
        setOtp([...otp.map((d, idx) => (idx === index ? element.value : d))]);
        if (element.nextSibling) {
            element.nextSibling.focus();
        }
    };
    const handleOTPSubmit = async () => {
        setLoading(true);
        const enteredOTP = otp.join("");
        if (enteredOTP.length < 6) {
            toast.error("Please enter a valid 6-digit OTP.");
            return;
        }

        try {
            const response = await axios.post("https://yodi-backend-1.onrender.com/api/user/verify-otp", { email: formData.email, otp: enteredOTP });
            toast.success("OTP verified successfully. Account created.");
            console.log(response)
            navigate("/login");
        } catch (error) {
            toast.error("Invalid OTP. Please try again.");
            console.log(error)
        } finally {
            setLoading(false);
        }
    };


    return (
        <div className='d-flex justify-content-center align-items-center mx-5 my-5'>
            <div className='bg-white border p-3 rounded' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: "150vh" }}>
                <div className='d-flex justify-content-center align-item-center'>
                    <a href=' '><img className='click_able_logo' src='./Assets/logoBlue.png' alt='clickable-logo'></img></a>
                </div>
                <div className='row' >
                    <div className=' col-md-6 d-none d-md-block'>
                        <img className='signup_side_image' src='./Assets/side_image.jpg' alt='side_image'></img>
                    </div>
                    <div className='col form-div'>
                        <form onSubmit={handleSubmit}>
                            <h3 className='text-center'>Welcome to <img className='logo' src='./Assets/logoBlue.png' alt='Yodi'></img>  Water Park</h3>
                            {formErrors.server && <div className="alert alert-danger">{formErrors.server}</div>}
                            <div className='mb-3'>
                                <label className='mb-2 text_font' htmlFor='email'>*Email</label>
                                <input
                                    type="email"
                                    placeholder='example@gmail.com'
                                    className={`form-control ${formErrors.email && 'is-invalid'} rounded-12`}
                                    value={formData.email}
                                    onChange={handleChange}
                                    name='email'
                                />
                                {formErrors.email && <div className='invalid-feedback'>{formErrors.email}</div>}
                            </div>
                            <div className='mb-3 form-group row text_font'>
                                <div className='col'>
                                    <label className='mb-2' htmlFor='firstname'>*First Name</label>
                                    <input
                                        type="name"
                                        placeholder='firstname'
                                        className={`form-control ${formErrors.firstName && 'is-invalid'} rounded-12`}
                                        value={formData.firstName}
                                        onChange={handleChange}
                                        name="firstName"
                                    />
                                    {formErrors.firstName && <div className='invalid-feedback'>{formErrors.firstName}</div>}
                                </div>
                                <div className='col text_font'>
                                    <label className='mb-2' htmlFor='lastname'>*Last Name</label>
                                    <input
                                        type="name"
                                        placeholder='lastName'
                                        className={`form-control ${formErrors.lastName && 'is-invalid'}`}
                                        value={formData.lastName}
                                        onChange={handleChange}
                                        name="lastName"
                                    />
                                    {formErrors.lastName && <div className='invalid-feedback'>{formErrors.lastName}</div>}
                                </div>
                            </div>
                            <div className='mb-3 text_font position-relative'>
                                <label className='password-input-container d-flex align-items-center mb-2' htmlFor='password'>Password</label>
                                <input
                                    type={formData.showPassword ? "text" : "password"}
                                    className={`form-control ${formErrors.password && 'is-invalid'}`}
                                    value={formData.password}
                                    onChange={handleChange}
                                    name="password"
                                />
                                <span className="password-toggle-icon" onClick={handleTogglePasswordVisibility}>
                                    {formData.showPassword ? <FaEyeSlash /> : <FaEye />}
                                </span>
                                {formErrors.password && <div className='invalid-feedback'>{formErrors.password}</div>}
                            </div>

                            <div className='mb-3 text_font position-relative'>
                                <label className='password-input-container d-flex align-items-center mb-2' htmlFor='confirmpassword'>Confirm Password</label>
                                <input
                                    type={formData.showConfirmPassword ? "text" : "password"}
                                    className={`form-control ${formErrors.confirmPassword && 'is-invalid'}`}
                                    value={formData.confirmPassword}
                                    onChange={handleChange}
                                    name="confirmPassword"
                                />
                                <span className="password-toggle-icon" onClick={handleToggleConfirmPasswordVisibility}>
                                    {formData.showConfirmPassword ? <FaEyeSlash /> : <FaEye />}
                                </span>
                                {formErrors.confirmPassword && <div className='invalid-feedback'>{formErrors.confirmPassword}</div>}
                            </div>
                            <div className='mt-4'>
                                <div className="form-check">
                                    <input
                                        type="checkbox"
                                        className={`form-check-input ${formErrors.termsAndConditions && 'is-invalid'}`}
                                        checked={formData.termsAndConditions}
                                        onChange={handleChange}
                                        name="termsAndConditions"
                                        id="termsAndConditions"
                                    />
                                    <label className="form-check-label custom-font-size" htmlFor="termsAndConditions">
                                        By creating an account, you agree to the <span style={{ textDecoration: 'underline' }}>Terms of Use</span> and <span style={{ textDecoration: 'underline' }}>Privacy Policy</span>.
                                    </label>
                                </div>
                                {formErrors.termsAndConditions && <div className='invalid-feedback'>{formErrors.termsAndConditions}</div>}
                            </div>
                            <div className="row mt-4 d-flex justify-content-around">
                                <p className="col-md-8 form-group mt-2 custom-font-size">Already have an account?<Link to="/login" style={{ textDecoration: "none" }}>  Log In</Link></p>
                                <div className="col-md-4 text-center d-flex justify-content-around">
                                    <button type='submit' className='button' disabled={loading}>
                                        {loading ? 'Creating...' : 'CREATE ACCOUNT'}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <Modal show={showOTPModal} onHide={() => setShowOTPModal(false)} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Enter OTP</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="d-flex justify-content-center">
                        {otp.map((data, index) => (
                            <input
                                className="otp-input"
                                type="text"
                                name="otp"
                                maxLength="1"
                                key={index}
                                value={data}
                                onChange={(e) => handleOTPChange(e.target, index)}
                                onFocus={e => e.target.select()}
                            />
                        ))}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button type='submit' onClick={handleOTPSubmit} className='button' disabled={loading}>
                        {loading ? 'Verifying...' : 'Verify OTP'}
                    </button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default Signup;
