import 'bootstrap/dist/css/bootstrap.min.css';
import { FaPhone, FaEnvelope, FaMapMarkedAlt } from 'react-icons/fa';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import { Accordion } from 'react-bootstrap';
import L from 'leaflet';
import "../css/help.css";
import toast from 'react-hot-toast';
import { useState } from "react";

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon-2x.png',
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
});

const Help = () => {
    const position = [26.9023393, 90.4500180];

    const [formData, setFormData] = useState({ name: "", email: "", subject: "", message: "" })
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value,
        }))
    }

    const notify = () => toast.success('form submitted successfully');
    const handleSubmit = async (e) => {
        e.preventDefault()
        console.log(formData)
        fetch("http://localhost:8080/api/user/sendmail", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(formData)
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch(error => {
                console.error("Error:", error)
            })
    }

    return (
        <>
            <div className="mt-5 p-5" >
                <div className="contact_us mt-5 d-flex flex-column w-100">
                    <div className="w-100" id="muha">
                        <div className="text-center mb-4">
                            <h2>Get in Touch with us</h2>
                        </div>
                        <div className="row mx-0 w-100">
                            <div className="col-md-4 col-sm-12 d-flex justify-content-center mb-3">
                                <div className="card text-center" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: '100%' }}>
                                    <div className="card-body p-5">
                                        <div className="h1">
                                            <FaPhone style={{ color: 'black', fontSize: "30px" }} />
                                        </div>
                                        <h3 className="card-title mb-2">Contact Us</h3>
                                        <p>
                                            +975 17592225 <br />
                                            +975 17918061
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12 d-flex justify-content-center mb-3">
                                <div className="card text-center" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: '100%' }}>
                                    <div className="card-body p-5">
                                        <div className="h1">
                                            <FaEnvelope style={{ color: 'black', fontSize: "30px" }} />
                                        </div>
                                        <h3 className="card-title mb-2">Write To Us</h3>
                                        <p>
                                            yodipark@gmail.com <br />
                                            parkyodi@gmail.com
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-12 d-flex justify-content-center mb-3">
                                <div className="card text-center" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: '100%' }}>
                                    <div className="card-body p-5">
                                        <div className="h1">
                                            <FaMapMarkedAlt style={{ color: 'black', fontSize: "30px" }} />
                                        </div>
                                        <h3 className="card-title mb-2">Address</h3>
                                        <p>
                                            YD Aqua Park <br /> Gelephu
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='d-flex w-100 flex-wrap'>
                    <div className='colume col-md-6 col-sm-12 p-5'>
                        <div className='accordion mt-1'>
                            <Accordion defaultActiveKey={['#']} alwaysOpen>
                                <div className="faq">
                                    <h2 className='mb-5'>FAQ</h2>

                                    <Accordion.Item eventKey="0">
                                        <Accordion.Header>How often can we use the attractions?</Accordion.Header >
                                        <Accordion.Body>
                                            You can use all our slides and pools as often and as long as you like. Tickets include access to all waterslide facilities, incl. tubes, and lounge for the full day.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                    <Accordion.Item eventKey="1">
                                        <Accordion.Header>Can I smoke in the waterpark?</Accordion.Header>
                                        <Accordion.Body>
                                            Since the water park is a public place, we don't promote smoking with respect to all visitors.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                    <Accordion.Item eventKey="2">
                                        <Accordion.Header>Can we take pictures in the park?</Accordion.Header>
                                        <Accordion.Body>
                                            Yes, we have many interesting scenic points where we encourage you to use your camera and to keep your memories and share them with friends.
                                        </Accordion.Body>
                                    </Accordion.Item>
                                </div>
                            </Accordion>
                        </div>
                        <div className='map mt-5 align-items-start'>
                            <div className='d-flex justify-content-center mb-5 flex-start' style={{ width: '100%' }}>
                                <div style={{ height: '300px', width: '300px' }}>
                                    <h2 className='mb-3'>Map</h2>
                                    <MapContainer
                                        center={position}
                                        zoom={13}
                                        style={{ height: '100%', width: '100%' }}
                                        attributionControl={false}
                                        zoomControl={false}
                                        dragging={false}
                                        doubleClickZoom={false}
                                        scrollWheelZoom={false}
                                    >
                                        <TileLayer
                                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                        />
                                        <Marker position={position}>
                                            <Popup>
                                                YoDi Aqua and Recreational Park. <br /> Come visit us!
                                            </Popup>
                                        </Marker>
                                    </MapContainer>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-6 col-12  mt-5'>
                        <h2 className="mb-3 ">Write to us</h2>
                        <form className='m-0' onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label for="name" className="form-label">Name</label>
                                <input value={formData.name} onChange={handleChange} type="text" name="name" className="form-control" id="name" required />
                            </div>
                            <div className="mb-3">
                                <label for="email" className="form-label">Email Address</label>
                                <input value={formData.email} onChange={handleChange} type="email" name="email" className="form-control" id="email" required />
                            </div>
                            <div className="mb-3">
                                <label for="subject" className="form-label">Subject</label>
                                <input value={formData.subject} onChange={handleChange} type="text" name="subject" className="form-control" id="subject" required />
                            </div>
                            <div className="mb-3">
                                <label for="message" className="form-label">Your Message</label>
                                <textarea value={formData.message} onChange={handleChange} name="message" className="form-control" id="message" rows="3" required></textarea>
                            </div>
                            <div>

                                <button onClick={notify} type="submit" className="button w-20">Submit</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}


export default Help;
