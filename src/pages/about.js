import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/aboutus.css';
import Herobanner from '../components/herobanner/aboutus';
import { FaCalendarCheck, FaCogs } from 'react-icons/fa';
import { toast } from "react-toastify";
import { Card } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Navigation, Pagination } from 'swiper/modules';

function AboutUs() {
    const [contents, setContents] = useState([]);
    useEffect(() => {
        fetchContents();
    }, []);

    const fetchContents = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/content?category=aboutus");
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            setContents(data);
        } catch (error) {
            toast.error("Failed to fetch contents");
        }
    };

    return (
        <div>
            <div>
                <Herobanner />
            </div>
            <div className="column mt-5 mb-5">
                {contents.map(content => (
                    <div className="mb-4 about_us" key={content._id}>
                        <Card className='d-flex flex-row flex-wrap border-0'>
                            {content.images && content.images.length > 0 && (
                                <Card.Img className='image-size border-radius' variant="top" src={content.images[0]} />
                            )}
                            <Card.Body className='col-sm-6 about_text'>
                                <Card.Text>{content.text}</Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                ))}
            </div>

            <div className="ourservice mb-5 text-center">
                <h4><strong style={{ fontSize: "20px" }}>Our Services</strong></h4>
            </div>
            <div className='container'>
                <div className='row justify-content-center'>
                    <div className='col-lg-6 col-md-6 mb-4'>
                        <div className='box2 rounded-3 p-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className='text-center mb-3'>
                                <FaCalendarCheck style={{ fontSize: "80px", color: "#1CACEA" }}></FaCalendarCheck>
                            </div>
                            <div className='text4'>
                                <h3 className='opt'>Effortless Booking</h3>
                                <p>Our online reservation system provides a secure platform for guests to effortlessly book water park activities and facilities. By offering a user-friendly interface, visitors can easily plan their visit, select desired attractions, and receive instant confirmation, ensuring a stress-free experience and peace of mind regarding availability.By offering a user-friendly interface, visitors can easily plan their visit, select desired attractions</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-6 col-md-6 mb-4'>
                        <div className='box2 rounded-3 p-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className='text-center mb-3'>
                                <FaCogs style={{ fontSize: "80px", color: "#1CACEA" }}></FaCogs>
                            </div>
                            <div className='text4'>
                                <h3 className='opt'>Park Optimization</h3>
                                <p>Our system effectively manages park capacity by optimizing available space and resources. It controls crowd movement to ensure a comfortable and safe experience for visitors while upholding stringent safety regulations. By efficiently handling these aspects, we prioritize visitor satisfaction and maintain compliance with safety standards for a seamless park experience.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row mt-5 mb-3'>
                    <div className='col text-center'>
                        <h4><strong style={{ fontSize: "20px" }}>Meet Our Team</strong></h4>
                    </div>
                </div>

                <div className="slide-container mb-2">
                    <Swiper
                        spaceBetween={30}
                        slidesPerView={1}
                        breakpoints={{
                            640: {
                                slidesPerView: 1,
                            },
                            768: {
                                slidesPerView: 2,
                            },
                            1024: {
                                slidesPerView: 3,
                            },
                        }}
                        navigation
                        pagination={{ clickable: true }}
                        modules={[Navigation, Pagination]}
                    >
                        <SwiperSlide className="team_card mb-2 " style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className="image-content">
                                <span className="overlay"></span>
                                <div className="card-image">
                                    <img src='./Assets/jungney.jpg' className='card-img' alt=''></img>
                                </div>
                            </div>
                            <div className="card-content">
                                <h2 className="name">Karma Yoezer Jungney</h2>
                                <p className="description">Jungney focuses on integrating advanced technologies to enhance security and user satisfaction in the booking process.</p>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="team_card mb-2" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className="image-content">
                                <span className="overlay"></span>
                                <div className="card-image">
                                    <img src='./Assets/pelden.jpg' className='card-img' alt=''></img>
                                </div>
                            </div>
                            <div className="card-content">
                                <h2 className="name">Pelden Wangchuk</h2>
                                <p className="description">Pelden ensures that our system is scalable and can handle peak times efficiently, contributing to overall system robustness.</p>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="team_card mb-2" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className="image-content">
                                <span className="overlay"></span>
                                <div className="card-image">
                                    <img src='./Assets/lunten.jpg' className='card-img' alt=''></img>
                                </div>
                            </div>
                            <div className="card-content">
                                <h2 className="name">Lungten Wangmo</h2>
                                <p className="description">Lunten focuses on maintaining system security and ensuring data privacy, making our platform reliable and trustworthy.</p>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="team_card mb-2" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className="image-content">
                                <span className="overlay"></span>
                                <div className="card-image">
                                    <img src='./Assets/namgay.jpg' className='card-img' alt=''></img>
                                </div>
                            </div>
                            <div className="card-content">
                                <h2 className="name">Namgay Wangchuk</h2>
                                <p className="description">Namgay is a key developer of our system, focusing on seamless user experience and efficient booking processes.</p>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className="team_card mb-2" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className="image-content">
                                <span className="overlay"></span>
                                <div className="card-image">
                                    <img src='./Assets/lhaki.jpg' className='card-img' alt=''></img>
                                </div>
                            </div>
                            <div className="card-content">
                                <h2 className="name">Lhaki Yangden</h2>
                                <p className="description">Lhaki specializes in optimizing park resources and ensuring smooth operations for an enhanced visitor experience.</p>
                            </div>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </div>
    );
}

export default AboutUs;
