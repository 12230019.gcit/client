import React from 'react';

function ParkRules() {
    return (
        <div >
            <h3 className="text-center" style={{ marginTop: "100px" }}>Rules and Regulations</h3>
            <div className='container' id="park-rules" style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                <div className='p-5'>
                    <p>1. The park is open from 09 AM to 05 PM for summer and 10: AM to 04: PM for Winter.</p>
                    <p>2. Park facilities are for resident guests and members only.</p>
                    <p>3. Children of guests / members under 12 years must be accompanied by an adult.</p>
                    <p>4. Entering the park under the influence of alcohol and drugs of any kind is strictly prohibited.</p>
                    <p>5. Person with any infectious or contagious diseases and women with periods are prohibited from using the pools.</p>
                    <p>6. Use of pools are at your own risk.</p>
                    <p>7. Use of swimming costumes is mandatory.</p>
                    <p>8. Use of swimming cap (must for ladies and gents with long hair) or tie up.</p>
                    <p>9. Swimming costumes on sale are available with guard on duty.</p>
                    <p>10. Kindly shower before entering the pool and after coming out of pool.</p>
                    <p>11. No diving permitted since pool is not deep.</p>
                    <p>12. No spitting / peeing in the pool.</p>
                    <p>13. Do not leave valuables unattended around the pool.</p>
                    <p>14. Consumption of food and beverages inside the pool is strictly prohibited.</p>
                    <p>15. Glass containers and sharp objects are not allowed in and around the pool area.</p>
                    <p>16. Report any disturbance to the attendant.</p>
                    <p>17. Evacuate the pool if there is a lightening.</p>
                    <p>18. Instruction of the attendant are for safety, please co-operate and comply.</p>
                    <p>19. The management reserves the right to deny the use of the park or evict from the pools premises to anyone who fails to comply with the above safety rules and regulations.</p>
                    <p>20. Park will not be held responsible for any injury, harm or loss of personal items that may result because of the use of this facility.</p>
                    <p>21. Management reserves the right to any changes in the rules and regulations without prior notice.</p>
                </div>
                <div>
                    <h3 className="mt-5 p-5 text-center">YOUR COOPERATION WOULD BE GREATLY APPRECIATED.</h3>
                </div>
            </div>
        </div>
    );
}

export default ParkRules;
