import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../css/Home.css"; // Make sure to include your custom CSS
import Herobanner from "../components/herobanner/herobanner";
import CustomCarousel from "../components/Carousel/Carousel"; // Adjust the path as necessary
import { FaCalendarCheck, FaCogs } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const YourComponent = () => {
    return (
        <>
            {/* Hero banner */}
            <div>
                <Herobanner />
            </div>

            {/* Card section */}
            <div className='container mt-5'>
                <div className='row justify-content-center'>
                    <div className='col-lg-6 col-md-12 mb-4'>
                        <div className='box2 rounded-3 p-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className='text-center mb-3'>
                                <FaCalendarCheck style={{ fontSize: "80px", color: "#1CACEA" }} />
                            </div>
                            <div className='text4'>
                                <h3 className='opt'>Effortless Booking</h3>
                                <p>Securely reserve water park activities and facilities online, ensuring hassle-free planning and availability confirmation.</p>
                            </div>
                            <div className="d-flex justify-content-center">
                                <Link to="/aboutus">
                                    <button className="view_button">
                                        View more
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-6 col-md-12 mb-4'>
                        <div className='box2 rounded-3 p-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                            <div className='text-center mb-3'>
                                <FaCogs style={{ fontSize: "80px", color: "#1CACEA" }} />
                            </div>
                            <div className='text4'>
                                <h3 className='opt'>Park Optimization</h3>
                                <p>System efficiently handles park capacity, crowd control, and safety compliance</p>
                            </div>
                            <div className="d-flex justify-content-center">
                                <Link to="/aboutus">
                                    <button className="view_button">
                                        View more
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Carousel */}
            <div className="mt-5">
                <CustomCarousel />
            </div>

            {/* Event section */}
            <div className="event mt-5">
                <div className="tex">
                    <h3><strong style={{ fontSize: "20px" }}>EVENTS</strong></h3>
                </div>
                <div className="tex1">
                    <h4 className="custom-font">Splash Extravaganza: A Day of Water-Fueled Fun!</h4>
                </div>
                <section className="bg-white text-dark p-5 text-start text-lg-center">
                    <div className="container">
                        <div className="row align-items-center justify-content-between mt-3">
                            <div className="col-lg-6 col-md-12 mb-4">
                                <img
                                    className="img-fluid mx-auto d-block image-size border-radius"
                                    src="../Assets/img_1.jpg"
                                    alt="Splash Extravaganza"
                                />
                            </div>
                            <div className="col-lg-6 col-md-12">
                                <p className="lead">
                                    Join us for a day filled with thrilling water rides,
                                    family-friendly activities, and refreshing moments under the
                                    sun. It's the ultimate aquatic adventure for all ages!
                                </p>
                                <Link to="/tickets">
                                    <button className="book_button pe-4 ps-4">
                                        BOOK
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </>
    );
};

export default YourComponent;
