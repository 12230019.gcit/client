import React, { useState, useEffect } from 'react';
import '../css/Ticket.css';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'


const URL = "http://localhost:8080/api/userTicket/create"

function BookTicket() {
    const [data, setData] = useState([]);
    const [selectedDate, setSelectedDate] = useState('');
    const [numTickets_1, setNumTickets_1] = useState(0);
    const [numTickets_2, setNumTickets_2] = useState(0);
    const [showOverlay, setShowOverlay] = useState(false);
    const [showOverlay_2, setShowOverlay_2] = useState(false);
    const [showOverlay_3, setShowOverlay_3] = useState(false);
    const [showOverlay_4, setShowOverlay_4] = useState(false);
    const [selectedBank, setSelectedBank] = useState('');
    const [journalNumber, setJournalNumber] = useState('');

    useEffect(() => {
        async function FeatchData() {
            try {
                const fetchticket = await axios.get(
                    "http://localhost:8080/api/tickets"
                );
                const response = fetchticket.data;
                setData(response);

            } catch (error) {
                console.log(error);
            }
        }
        FeatchData();
    }, []);


    const [formData, setFormData] = useState({
        name: "",
        cid: "",
        phoneNumber: "",
        email: ""
    });

    const [ticketData, setTicketData] = useState({
        selectedDate: "",
        numTickets_1: "",
        numTickets_2: "",
        totalAmount: "",
        journalNumber: ""
    });

    const [formErrors, setFormErrors] = useState({});

    const ticketPrice_1 = 400;
    const ticketPrice_2 = 200;

    const totalTicketPrice_1 = (numTickets_1 * ticketPrice_1);
    const totalTicketPrice_2 = (numTickets_2 * ticketPrice_2);

    const totalAmount = (numTickets_1 * ticketPrice_1) + (numTickets_2 * ticketPrice_2);

    const hasSelectedTickets = numTickets_1 > 0 || numTickets_2 > 0;

    const handleInput = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleDateChange = (event) => {
        const { name, value } = event.target;
        setTicketData({
            ...ticketData,
            [name]: value
        });
        setSelectedDate(event.target.value);
    };

    const handleNumTicketsChange_1 = (event) => {
        const { name, value } = event.target;
        setTicketData({
            ...ticketData,
            [name]: value
        });
        setNumTickets_1(event.target.value);
        const newTotalAmount = (event.target.value * ticketPrice_1) + (numTickets_2 * ticketPrice_2);
        setTicketData(prevState => ({
            ...prevState,
            totalAmount: newTotalAmount
        }));
    };

    const handleNumTicketsChange_2 = (event) => {
        const { name, value } = event.target;
        setTicketData({
            ...ticketData,
            [name]: value
        });
        setNumTickets_2(event.target.value);
        const newTotalAmount = (numTickets_1 * ticketPrice_1) + (event.target.value * ticketPrice_2);
        setTicketData(prevState => ({
            ...prevState,
            totalAmount: newTotalAmount
        }));
    };

    const handleBankChange = (event) => {
        setSelectedBank(event.target.value);
    };

    const handleJournalNumberChange = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        })
        setJournalNumber(value);
        setTicketData(prevState => ({
            ...prevState,
            journalNumber: value
        }));
    };


    const validateForm = (step) => {
        const errors = {};

        if (step >= 1) {
            if (!formData.name.trim()) {
                errors.name = "Your name is required";
            }

            if (!formData.cid.trim()) {
                errors.cid = "CID is required";
            } else if (!formData.cid.match(/^\d{1,11}$/)) {
                errors.cid = "Invalid CID. It should be a number with maximum 11 digits.";
            }

            if (!formData.phoneNumber.trim()) {
                errors.phoneNumber = "Phone number is required";
            } else if (!formData.phoneNumber.match(/^(17|77|16)\d{6}$/)) {
                errors.phoneNumber = "Enter a valid phone number starting with 17, 77, or 16 followed by 6 digits";
            }

            if (!formData.email.trim()) {
                errors.email = "Email is required";
            } else if (!formData.email.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
                errors.email = "Enter a valid email address";
            }
        }

        if (step >= 2) {
            if (!journalNumber.trim()) {
                errors.journalNumber = "Journal number is required";
            } else {
                if (selectedBank === "BOB" && !journalNumber.match(/^\d{7}$/)) {
                    errors.journalNumber = "BOB journal number must be exactly 7 digits";
                } else if ((selectedBank === "BNB" || selectedBank === "DK Bank") && !journalNumber.match(/^\d{12}$/)) {
                    errors.journalNumber = `Journal number for ${selectedBank} must be exactly 12 digits`;
                }
            }
        }

        setFormErrors(errors);

        return Object.keys(errors).length === 0;
    };


    const handleOverlay = () => {
        const isValid = validateForm(1);
        console.log('isValid:', isValid);
        console.log('showOverlay:', showOverlay);
        console.log('showOverlay_2:', showOverlay_2);
        if (isValid) {
            setShowOverlay(false);
            setShowOverlay_2(true);
        }
    };

    const handleOverlayBook = () => {
        const isValid = validateForm(1);
        if (isValid) {
            setShowOverlay_2(false);
            setShowOverlay_3(true);
        }

    };


    const handleOverlayPay = () => {
        const isValid = validateForm(1); // Validate all steps including the final payment step
        if (isValid) {
            setShowOverlay_3(false);
            setShowOverlay_4(true);
        }
    };

    const handleSubmit = async () => {
        const isValid = validateForm(2);
        if (isValid) {
            const dataToSubmit = {
                ...ticketData,
                ...formData,
                selectedDate,
                numTickets_1,
                numTickets_2,
                totalAmount,
                journalNumber

            }

            setShowOverlay_4(false);

            try {
                const response = await axios.post(URL, dataToSubmit);
                Swal.fire({
                    title: "Journal number successfully Sent. ",
                    text: `ticket reservation will be confirmed via ${formData.email}`,
                    icon: "success",
                    confirmButtonText: "OK"
                });
                console.log(response);
            } catch (error) {

                Swal.fire({
                    title: "Oops!",
                    text: "Please try again later.",
                    icon: "error",
                    confirmButtonText: "OK"
                });
                console.log(error);
            }
        }
    };

    return (
        <div className="container my-5">
            <div className='row justify-content-center'>
                {data.tickets?.map((elem, index) => (
                    <div className="col-12 col-md-12 my-5" key={index}>
                        <div className="bg-white border rounded  mx-auto ticket-container">
                            <div className="blue-line mb-3"></div>
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-1">
                                    <p>Name: {elem.name}</p>
                                    <p>Age: {elem.age}</p>
                                    <p>Price: {elem.price}</p>
                                </div>
                                <div className='me-3'>
                                    <img className='img-fluid ticket-logo me-4' src='./Assets/logoBlue.png' alt='logo'></img>
                                </div>
                                <div className="ticket_bar"></div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <div>
                <form>
                    <div className="row my-5">
                        <div className='col-lg-6 '>
                            <div className='bg-white border p-3 rounded m-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                                <h6 className='text-center'>BOOK TICKET</h6>
                                <div className="underline mt-2"></div>
                                <div className="row mt-3">
                                    <div className='col'>
                                        <label className='mb-2  text_font' htmlFor="date">Select Date</label>
                                        <input
                                            type="date"
                                            id="date"
                                            value={ticketData.selectedDate}
                                            onChange={handleDateChange}
                                            className="form-control"
                                            name='selectedDate'
                                        />
                                    </div>
                                </div>
                                <div className="row mt-5">
                                    <h6>Select the number of tickets</h6>
                                    <div className='col mt-3'>
                                        <label className='mb-2 text_font' htmlFor="tickets_1">Yodi Splash Pass </label>
                                        <input
                                            type="number"
                                            id="tickets_1"
                                            value={ticketData.numTickets_1}
                                            onChange={handleNumTicketsChange_1}
                                            className="form-control"
                                            name='numTickets_1'
                                        />
                                    </div>
                                    <div className='col mt-3'>
                                        <label className='mb-2 text_font' htmlFor="tickets_2">Yodi Splash Pass</label>
                                        <input
                                            type="number"
                                            id="tickets_2"
                                            value={ticketData.numTickets_2}
                                            onChange={handleNumTicketsChange_2}
                                            className="form-control"
                                            name='numTickets_2'
                                        />
                                    </div>
                                </div>
                                <div className="d-flex justify-content-end mt-4">
                                    <button
                                        type="button"
                                        className="button pe-4 ps-4 "
                                        onClick={() => setShowOverlay(true)}
                                        disabled={!hasSelectedTickets}
                                    >
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='bg-white border p-3 rounded m-4' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                                <div className="text-end">
                                    Date: {selectedDate}
                                </div>
                                <h6 className='text-center mt-2'>Entry Ticket Details</h6>
                                <div className="underline "></div>
                                {hasSelectedTickets && (
                                    <div className="row mt-3">
                                        <div className='col'>
                                            <div className='mt-3'>
                                                <p>Yodi Adult Pass :  {numTickets_1} * {ticketPrice_1} = {totalTicketPrice_1} </p>
                                            </div>
                                            <div className='mt-3'>
                                                <p>Yodi Child Pass :  {numTickets_2} * {ticketPrice_2} = {totalTicketPrice_2} </p>
                                            </div>
                                            <div className="underline "></div>
                                            <div className='mt-3'>

                                                <label htmlFor="totalAmount" className='mb-2'>Total Amount</label>
                                                <input
                                                    type="number"
                                                    id="totalAmount"
                                                    className="form-control"
                                                    value={totalAmount}
                                                    name="totalAmount"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>

                </form>
                {/* ENTER YOU DETAILS */}
                <Modal show={showOverlay} onHide={() => setShowOverlay(false)} centered>
                    <Modal.Header>
                        <Modal.Title>Enter Your Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form>
                            <div className="row">
                                <div className="col">
                                    <label className="mb-2 text_font" htmlFor="name">Name</label>
                                    <input
                                        type="string"
                                        id="name"
                                        name="name"
                                        value={formData.name}
                                        onChange={handleInput}
                                        className={`form-control ${formErrors.name ? 'is-invalid' : ''}`}
                                    />
                                    {formErrors.name && (
                                        <div className="invalid-feedback">{formErrors.name}</div>
                                    )}
                                </div>
                                <div className="col">
                                    <label className="mb-2 text_font" htmlFor="cid">CID</label>
                                    <input
                                        type="number"
                                        id="cid"
                                        name="cid"
                                        value={formData.cid}
                                        onChange={handleInput}
                                        className={`form-control ${formErrors.cid ? 'is-invalid' : ''}`}
                                    />
                                    {formErrors.cid && (
                                        <div className="invalid-feedback">{formErrors.cid}</div>
                                    )}
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col">
                                    <label className="mb-2 text_font" htmlFor="phoneNumber">Phone Number</label>
                                    <input
                                        type="number"
                                        id="phoneNumber"
                                        name="phoneNumber"
                                        value={formData.phoneNumber}
                                        onChange={handleInput}
                                        className={`form-control ${formErrors.phoneNumber ? 'is-invalid' : ''}`}
                                    />
                                    {formErrors.phoneNumber && (
                                        <div className="invalid-feedback">{formErrors.phoneNumber}</div>
                                    )}
                                </div>
                                <div className="col">
                                    <label className="mb-2 text_font" htmlFor="email">Email</label>
                                    <input
                                        type="string"
                                        id="email"
                                        name="email"
                                        value={formData.email}
                                        onChange={handleInput}
                                        className={`form-control ${formErrors.email ? 'is-invalid' : ''}`}
                                    />
                                    {formErrors.email && (
                                        <div className="invalid-feedback">{formErrors.email}</div>
                                    )}
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <button
                            type="button"
                            className="custom-btn  w-100 pe-4 ps-4"
                            onClick={() => setShowOverlay(false)}
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="button pe-4 ps-4"
                            onClick={handleOverlay}
                        >
                            Next
                        </button>
                    </Modal.Footer>
                </Modal>
                {/* CONFIRMATION */}
                <Modal show={showOverlay_2} onHide={() => setShowOverlay_2(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure of the ticket purchase details?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form>
                            <div className='row mb-5'>
                                <div className='col bg-white border p-3 rounded m-4 ' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                                    <h5 className='text-center'>Booking Details</h5>
                                    <hr className='underline mt-4 mb-3' />
                                    <p><strong>Date:</strong> {selectedDate}</p>
                                    <p><strong>Yodi Surge Pass:</strong> {numTickets_1}</p>
                                    <p><strong>Yodi Splash Pass:</strong> {numTickets_2}</p>
                                    <p><strong>Total Amount: Nu:    </strong>  {totalAmount}</p>
                                    <hr className='underline mt-4 mb-3' />
                                </div>
                                <div className='col bg-white border p-3 rounded m-4 ' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}>
                                    <h5 className='text-center'>Entry Tickets Details</h5>
                                    <hr className='underline mt-4 mb-3' />
                                    <p><strong>Name:</strong> {formData.name}</p>
                                    <p><strong>CID/Permit:</strong>{formData.cid}</p>
                                    <p><strong>PhoneNumber:</strong>{formData.phoneNumber}</p>
                                    <p><strong>Email:</strong>{formData.email}</p>
                                    <hr className='underline mt-4 mb-3' />
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <button
                            type="button"
                            className="custom-btn  w-100 pe-4 ps-4"
                            onClick={() => setShowOverlay_2(false)}
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="button pe-4 ps-4"
                            onClick={handleOverlayBook}
                        >
                            NEXT
                        </button>
                    </Modal.Footer>
                </Modal>
                {/* PAYMENT */}
                <Modal show={showOverlay_3} onHide={() => setShowOverlay_3(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='p-3 rounded m-3 text-center' style={{ fontSize: "20px", fontWeight: "bold" }}>
                            SELECT A PAYMENT METHOD
                        </div>
                        <div className='row'>
                            <div className="col-lg-12 mt-3"> {/* Right align the button */}
                                <button
                                    className='button'
                                    style={{ width: "100%" }}
                                    onClick={handleOverlayPay}
                                >
                                    LOCAL BANK
                                </button>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                    {/* PAYMETN INFORMATION */}
                </Modal>
                <Modal show={showOverlay_4} onHide={() => setShowOverlay_4(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Payment Information</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            {/* Left Column */}
                            <div className='col-6'>
                                <div className="mt-5">
                                    <label htmlFor="dropDown" className="form-label">Select Your Bank Type</label>
                                    <select className="form-select text-center" id="dropDown" value={selectedBank} onChange={handleBankChange}>
                                        <option value="BOB">Bank of Bhutan</option>
                                        <option value="BNB">Bhutan National Bank</option>
                                        <option value="DK Bank">DK Bank</option>
                                    </select>
                                </div>
                                {/* Label with input for journal number */}
                                <div className="mt-5">
                                    <label htmlFor="Number" className="form-label">Enter Your Journal Number</label>
                                    <input
                                        type="number"
                                        className={`"form-control ${formErrors.journalNumber ? "is-invalid" : ""} text-center"`}
                                        style={{ width: "100%" }}
                                        id="journalNumber"
                                        placeholder='Journal Number'
                                        value={ticketData.journalNumber}
                                        onChange={handleJournalNumberChange}
                                    />
                                    {formErrors.journalNumber && (
                                        <div className="invalid-feedback">{formErrors.journalNumber}</div>
                                    )}
                                </div>
                            </div>
                            {/* Right Column */}
                            <div className='col-6'>
                                <img src="./Assets/logoBlue.png" alt="QR Code" className="img-fluid mb-3" style={{ height: "25px", width: "50px", marginLeft: "80px" }} />
                                <img src="./Assets/QRcode.jpg" alt="QR Code" className="img-fluid" />
                                <p className='text-center mt-2' style={{ fontWeight: "bold" }}>YODI WATER PARK</p>
                                <p className='text-center' style={{ fontWeight: "bold" }}>205105109585</p>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button
                            type="button"
                            className="custom-btn  w-100 pe-4 ps-4"
                            onClick={() => setShowOverlay_3(false)}
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="button pe-4 ps-4"
                            onClick={handleSubmit} Call handleSubmit function here
                        >
                            Pay
                        </button>
                    </Modal.Footer>
                </Modal>

            </div>
        </div>
    );
}

export default BookTicket;

