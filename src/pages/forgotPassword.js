import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { toast } from "react-toastify";
import { Link, useNavigate } from 'react-router-dom';

const ForgotPassword = () => {
    const [showOTPModal, setShowOTPModal] = useState(false);
    const [showResetPasswordModal, setShowResetPasswordModal] = useState(false);
    const [email, setEmail] = useState("");
    const [otp, setOtp] = useState(["", "", "", "", "", ""]);
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [token, setToken] = useState("");
    const navigate = useNavigate();

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handleOTPChange = (element, index) => {
        const value = element.value;
        if (isNaN(value)) return;
        setOtp([...otp.map((d, idx) => (idx === index ? value : d))]);
        if (element.nextSibling && value) {
            element.nextSibling.focus();
        }
    };

    const handleNewPasswordChange = (e) => {
        setNewPassword(e.target.value);
    };

    const handleConfirmPasswordChange = (e) => {
        setConfirmPassword(e.target.value);
    };

    const handleEmailSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        try {
            const response = await fetch('http://localhost:8080/api/user/forgotPassword', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            });

            if (response.ok) {
                setShowOTPModal(true);
            } else {
                const data = await response.json();
                console.error("Email Submit Error:", data.message);
                toast.error("Failed to send OTP. Please try again.");
            }
        } catch (error) {
            console.error('Email Submit Error:', error);
            toast.error("An error occurred. Please try again.");
        } finally {
            setLoading(false);
        }
    };

    const handleOTPSubmit = async (e) => {
        e.preventDefault();
        const enteredOTP = otp.join('');
        setLoading(true);

        try {
            const response = await fetch('http://localhost:8080/api/user/verify-otp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email, otp: enteredOTP }),
            });

            if (response.ok) {
                const data = await response.json();
                setToken(data.token);
                setShowOTPModal(false);
                setShowResetPasswordModal(true);
            } else {
                const data = await response.json();
                console.error("OTP Submit Error:", data.message);
                toast.error("Failed to verify OTP. Please try again.");
            }
        } catch (error) {
            console.error('OTP Submit Error:', error);
            toast.error("An error occurred. Please try again.");
        } finally {
            setLoading(false);
        }
    };

    const handleResetPassword = async (e) => {
        e.preventDefault();
        if (newPassword !== confirmPassword) {
            toast.error("Passwords do not match. Please try again.");
            return;
        }
        setLoading(true);

        try {
            const response = await fetch("http://localhost:8080/api/user/resetPassword", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ token, newPassword, confirmPassword }),
            });

            if (response.ok) {
                toast.success("Password reset successful. Please log in with your new password.");
                navigate('/login');
            } else {
                const data = await response.json();
                console.error("Password Reset Error:", data.message);
                toast.error("Failed to reset password. Please try again.");
            }
        } catch (error) {
            console.error('Password Reset Error:', error);
            toast.error("An error occurred. Please try again.");
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className='d-flex justify-content-center align-items-center mx-5 my-5'>
            <div className='bg-white border p-3 rounded' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: "60vh" }}>
                <form onSubmit={handleEmailSubmit}>
                    <div className='d-flex justify-content-center align-item-center'>
                        <img className='click_able_logo' src='./Assets/logoBlue.png' alt='clickable-logo'></img>
                    </div>
                    <h3 className='text-center mt-3 mb-4' style={{ fontWeight: "bold" }}>Reset your password</h3>
                    <p className='text-center mb-4'>Enter your email address and we will send you instructions to reset your password.</p>
                    <div className='mb-4'>
                        <label className='mb-2'>Email Address</label>
                        <input
                            id="email"
                            type='email'
                            className='form-control'
                            value={email}
                            onChange={handleEmailChange}
                            required
                        />
                    </div>
                    <div className='row d-flex justify-content-between'>
                        <button type='submit' className='button ps-5 pe-5 mt-3' disabled={loading}>
                            {loading ? 'Sending...' : 'Continue'}
                        </button>
                        <Link className='text-center mb-2 mt-3' to="/login" style={{ textDecoration: "none", fontWeight: "bold", color:"#1CACEA"}}>Back to YODI WATER PARK web</Link>
                    </div>
                </form>
            </div>

            <Modal show={showOTPModal} onHide={() => setShowOTPModal(false)} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Please Enter the OTP</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleOTPSubmit}>
                        <div className='d-flex justify-content-center'>
                            {otp.map((data, index) => (
                                <input
                                    className="otp-input"
                                    type="text"
                                    maxLength="1"
                                    key={index}
                                    value={data}
                                    onChange={e => handleOTPChange(e.target, index)}
                                    onFocus={e => e.target.select()}
                                    style={{
                                        width: '50px',
                                        height: '50px',
                                        margin: '0 10px',
                                        fontSize: '20px',
                                        textAlign: 'center',
                                        borderRadius: '5px',
                                        border: '1px solid #ccc'
                                    }}
                                />
                            ))}
                        </div>
                        <div className='row d-flex justify-content-between mt-3'>
                            <button type='button' className='cancel_button mb-3' onClick={() => navigate('/login')}>
                                Back
                            </button>
                            <button type='submit' className='button' disabled={loading}>
                                {loading ? 'Verifying...' : 'Submit OTP'}
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>

            <Modal show={showResetPasswordModal} onHide={() => setShowResetPasswordModal(false)} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Reset Your Password</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleResetPassword}>
                        <div className='mb-3'>
                            <label>New Password*</label>
                            <input
                                id="newPassword"
                                type='password'
                                className='form-control'
                                value={newPassword}
                                onChange={handleNewPasswordChange}
                                required
                            />
                        </div>
                        <div className='mb-3'>
                            <label>Re-enter New Password*</label>
                            <input
                                id="confirmPassword"
                                type='password'
                                className='form-control'
                                value={confirmPassword}
                                onChange={handleConfirmPasswordChange}
                                required
                            />
                        </div>
                        <div className='row d-flex justify-content-between mt-3'>
                            <button type='button' className='cancel_button mb-3' onClick={() => navigate('/login')}>
                                CANCEL
                            </button>
                            <button type='submit' className='button' disabled={loading}>
                                {loading ? 'Resetting...' : 'Reset Password'}
                            </button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        </div>
    );
};

export default ForgotPassword;
