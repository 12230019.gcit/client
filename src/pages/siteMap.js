import React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const Sitemap = () => {
    return (
        <div className="container" style={{ marginTop: "100px" }}>
            <div><strong style={{ fontSize: "30px" }}>Yodi Site Map</strong>
                <div className=" blue-line-3"></div>
            </div>
            <div className="row mt-5">
                <div className="col-3">
                    <h5 style={{ fontWeight: "bold" }}>About Yodi</h5>
                    <ul className="row">
                        <Link to="/aboutus" style={{ textDecoration: "none", color: "black" }}>About Us</Link>
                        <Link to="/help" style={{ textDecoration: "none", color: "black" }}>Our Services</Link>
                    </ul>
                </div>
                <div className="col-3">
                    <h5 style={{ fontWeight: "bold" }}>Attractions</h5>
                    <ul className="row">
                        <Link to="/attraction" style={{ textDecoration: "none", color: "black" }}>Attractions</Link>
                        <Link to="/home" style={{ textDecoration: "none", color: "black" }}>Events</Link>
                    </ul>
                </div>
                <div className="col-3">
                    <h5 style={{ fontWeight: "bold" }}>Tickets</h5>
                    <ul className="row">
                        <Link to="/tickets" style={{ textDecoration: "none", color: "black" }}>Tickets</Link>
                        <Link to="/tickets" style={{ textDecoration: "none", color: "black" }}>Book Now</Link>
                    </ul>
                </div>
                <div className="col-3">
                    <h5 style={{ fontWeight: "bold" }}>Help</h5>
                    <ul className="row">
                        <Link to="/help" style={{ textDecoration: "none", color: "black" }}>FAQs</Link>
                        <Link to="/help" style={{ textDecoration: "none", color: "black" }}>Feedbacks</Link>
                        <Link to="/help" style={{ textDecoration: "none", color: "black" }}>Maps</Link>
                        <Link to="/help" style={{ textDecoration: "none", color: "black" }}>Contacts us</Link>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Sitemap;
