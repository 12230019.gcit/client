import React, { useEffect, useState } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { toast } from "react-toastify";
import Herobanner from "../components/herobanner/attraction";
import "../css/content.css";

const Attractions = () => {
  const [contents, setContents] = useState([]);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    fetchContents();
  }, []);

  const fetchContents = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/content?category=attractions");
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      setContents(data);
    } catch (error) {
      toast.error("Failed to fetch contents");
    }
  };

  const handlePrev = () => {
    setActiveIndex((prevIndex) => (prevIndex === 0 ? 3 : prevIndex - 1));
  };

  const handleNext = () => {
    setActiveIndex((prevIndex) => (prevIndex === 3 ? 0 : prevIndex + 1));
  };

  return (
    <div>
      <div>
        <Herobanner />
      </div>
      <div className="container mt-5 mb-4">
        <div className="wrapper">
          <button className="nav-button prev" onClick={handlePrev}>‹</button>
          <div className="attraction_container mb-4">
            <input type="radio" name="slide" id="c1" checked={activeIndex === 0} onChange={() => setActiveIndex(0)} style={{ display: "none" }} />
            <label htmlFor="c1" className="attract_card">
              <Row>
                <Col className="attract_description">
                  <p>Escape the heat and embrace refreshing waters amidst stunning sunset vistas at our water park.</p>
                </Col>
              </Row>
            </label>
            <input type="radio" name="slide" id="c2" checked={activeIndex === 1} onChange={() => setActiveIndex(1)} style={{ display: "none" }} />
            <label htmlFor="c2" className="attract_card">
              <Row>
                <Col className="attract_description">
                  <p>Lounging by the poolside  where relaxation meets tranquility, sun kisses your skin and the pool cradles your dreams.</p>
                </Col>
              </Row>
            </label>
            <input type="radio" name="slide" id="c3" checked={activeIndex === 2} onChange={() => setActiveIndex(2)} style={{ display: "none" }} />
            <label htmlFor="c3" className="attract_card">
              <Row>
                <Col className="attract_description">
                  <p>Where every splash is a symphony of joy. Making a splash and memories that'll
                    last a lifetime.</p>
                </Col>
              </Row>
            </label>
            <input type="radio" name="slide" id="c4" checked={activeIndex === 3} onChange={() => setActiveIndex(3)} style={{ display: "none" }} />
            <label htmlFor="c4" className="attract_card">
              <Row>
                <Col className="attract_description">
                  <p>Overview of the water park’s attractions. Let the waves of laughter and joy wash over you in our water park's epic swimming pool.</p>
                </Col>
              </Row>
            </label>
          </div>
          <button className="nav-button next" onClick={handleNext}>›</button>
        </div>
        <Row>
          {/* {contents.map(content => (
            <Col md={6} lg={4} className="mb-4" key={content._id}>
              <Card className="h-100">
                {content.images && content.images.length > 0 && (
                  <Card.Img variant="top" src={content.images[0]} />
                )}
                <Card.Body>
                  <Card.Text>{content.text}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))} */}
          <div className="column mt-5">
            {contents.map(content => (
              <div className="mb-4 justify-content-center" key={content._id}>
                <Card className='d-flex flex-row flex-wrap border-0'>
                  {content.images && content.images.length > 0 && (
                    <Card.Img className='image-size border-radius' variant="top" src={content.images[0]} />
                  )}
                  <Card.Body className='col-sm-6 about_text'>
                    <Card.Text>{content.text}</Card.Text>
                  </Card.Body>
                </Card>
              </div>
            ))}
          </div>
        </Row>
      </div></div>
  );
};

export default Attractions;



