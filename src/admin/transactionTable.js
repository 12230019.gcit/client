import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from "react-toastify";
import { Container, Table, Alert, Button } from 'react-bootstrap';

const TransactionTable = () => {
    const [transactions, setTransactions] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetchTransactions();
    }, []);

    const fetchTransactions = () => {
        axios.get('http://localhost:8080/api/userTicket')
            .then(response => {
                setTransactions(response.data);
            })
            .catch(error => {
                setError(error);
                console.error('Error fetching transactions:', error);
            });
    };

    const confirmTransaction = (id) => {
        if (!id) {
            console.error('Invalid transaction ID:', id);
            setError('Invalid transaction ID');
            return;
        }
        console.log('Updating transaction with ID:', id);

        axios.put(`http://localhost:8080/api/userTicket/${id}`, { confirmed: true })
            .then(response => {
                console.log('Transaction confirmed:', response.data);
                setTransactions(prevTransactions =>
                    prevTransactions.map(transaction =>
                        transaction._id === id ? { ...transaction, confirmed: true } : transaction
                    )
                );
                toast.success("Confirmed Successfully");
            })
            .catch(error => {
                setError(error.response ? error.response.data.message : error.message);
                console.error('Error confirming transaction:', error);
            });
    };

    return (
        <Container className="mt-5">
            <h2>Transaction Table</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Table bordered hover responsive>
                <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th>Reservation Holder</th>
                        <th>CID</th>
                        <th>Tickets Details</th>
                        <th>Amount</th>
                        <th>Contact Info</th>
                        <th>Journal Number</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {transactions.map((transaction, index) => (
                        <tr key={transaction._id || index}>
                            <td>{index + 1}</td>
                            <td>{transaction.name}</td>
                            <td>{transaction.cid}</td>
                            <td className='text-center'>
                                {transaction.numTickets_1 > 0 && (
                                    <div>Adult: {transaction.numTickets_1}</div>
                                )}
                                {transaction.numTickets_2 > 0 && (
                                    <div>Child: {transaction.numTickets_2}</div>
                                )}
                            </td>
                            <td>{<div>Nu:/ {transaction.totalAmount}</div>}</td>
                            <td>
                                {transaction.email && (
                                    <div>Email: {transaction.email}</div>
                                )}
                                {transaction.phoneNumber && (
                                    <div>Phone Number: {transaction.phoneNumber}</div>
                                )}
                            </td>
                            <td>{transaction.journalNumber}</td>
                            <td>
                                {transaction.confirmed ? (
                                    <Button variant="success" disabled>Approved</Button>
                                ) : (
                                    <Button
                                        onClick={() => confirmTransaction(transaction._id)}
                                        variant="warning"
                                    >
                                        Confirm
                                    </Button>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    );
};

export default TransactionTable;
