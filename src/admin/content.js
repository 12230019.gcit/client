import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { Button, Modal, Form, Card } from 'react-bootstrap';
// import './css/content.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

const ContentUpload = () => {
    const [contents, setContents] = useState([]);
    const [formData, setFormData] = useState({
        id: null,
        text: "",
        images: null,
        category: ""
    });
    const [showModal, setShowModal] = useState(false);
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        fetchContents();
    }, []);

    const fetchContents = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/content");
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            setContents(data);
        } catch (error) {
            toast.error("Failed to fetch contents");
        }
    };

    const handleFileChange = (event) => {
        setFormData({ ...formData, images: event.target.files });
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
            return;
        }
        setValidated(true);

        try {
            const contentData = new FormData();
            if (formData.text) {
                contentData.append("text", formData.text);
            }
            if (formData.images) {
                for (let i = 0; i < formData.images.length; i++) {
                    contentData.append("images", formData.images[i]);
                }
            }
            if (formData.category) {
                contentData.append("category", formData.category);
            }

            const method = formData.id ? "PUT" : "POST";
            const url = formData.id
                ? `http://localhost:8080/api/content/${formData.id}`
                : "http://localhost:8080/api/content";

            const response = await fetch(url, {
                method: method,
                body: contentData,
            });

            if (response.ok) {
                toast.success(`Content ${formData.id ? "updated" : "uploaded"} successfully`);
                setShowModal(false);
                setFormData({
                    id: null,
                    text: "",
                    images: null,
                    category: ""
                });
                fetchContents();
            } else {
                const errorData = await response.json();
                toast.error(errorData.message);
            }
        } catch (error) {
            // toast.error("An error occurred while uploading the content");
            toast.error("NOt from here");
        }
    };

    const handleEdit = (content) => {
        setFormData({
            id: content._id,
            text: content.text,
            images: null,
            category: content.category
        });
        setShowModal(true);
    };

    const handleDelete = async (id) => {
        if (!window.confirm("Are you sure you want to delete this content?")) {
            return;
        }

        try {
            const response = await fetch(`http://localhost:8080/api/content/${id}`, {
                method: "DELETE",
            });

            if (response.ok) {
                toast.success("Content deleted successfully");
                fetchContents();
            } else {
                const errorData = await response.json();
                toast.error(errorData.message);
            }
        } catch (error) {
            toast.error("An error occurred while deleting the content");
        }
    };

    return (
        <div className="container mt-5 mb-4">
            <div className="d-flex justify-content-end mb-3">
                <Button onClick={() => {
                    setFormData({ id: null, text: "", images: null, category: "" });
                    setShowModal(true);
                }}>Upload Content</Button>
            </div>

            <Modal show={showModal} onHide={() => setShowModal(false)}>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formData.id ? "Update" : "Upload"} Content</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="formText">
                            <Form.Label>Text</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter text"
                                name="text"
                                value={formData.text}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please provide some text.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formImages">
                            <Form.Label>Images</Form.Label>
                            <Form.Control
                                type="file"
                                name="images"
                                multiple
                                onChange={handleFileChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please upload images.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formCategory">
                            <Form.Label>Category</Form.Label>
                            <Form.Control
                                as="select"
                                name="category"
                                value={formData.category}
                                onChange={handleChange}
                                required
                            >
                                <option value="">Select Category</option>
                                <option value="aboutus">About Us</option>
                                <option value="attractions">Attractions</option>
                            </Form.Control>
                            <Form.Control.Feedback type="invalid">
                                Please select a category.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => setShowModal(false)}>
                            Cancel
                        </Button>
                        <Button type="submit" variant="primary">
                            {formData.id ? "Update" : "Upload"}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            <div className="row">
                {contents.map(content => (
                    <div className="col-md-4 mb-4" key={content._id}>
                        <Card>
                            {content.images && content.images.length > 0 && (
                                <Card.Img variant="top" src={content.images[0]} />
                            )}
                            <Card.Body>
                                <Card.Text>{content.text}</Card.Text>
                                <div className="d-flex justify-content-between">
                                    <Button className="custom-bg" onClick={() => handleEdit(content)}>
                                        <i className="fas fa-edit"></i> Edit
                                    </Button>
                                    <Button variant="danger" onClick={() => handleDelete(content._id)}>
                                        <i className="fas fa-trash-alt"></i> Delete
                                    </Button>
                                </div>
                            </Card.Body>
                        </Card>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ContentUpload;