import React, { useEffect, useState } from 'react';
import { Card, Button, Modal, Form } from 'react-bootstrap';
import { toast } from "react-toastify";
import "../css/admin/content.css"

const Attraction = () => {
    const [contents, setContents] = useState([]);
    const [formData, setFormData] = useState({
        id: null,
        text: "",
        images: null,
        category: "attractions"
    });
    const [showModal, setShowModal] = useState(false);
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        fetchContents();
    }, []);

    const fetchContents = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/content?category=attractions");
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            setContents(data);
        } catch (error) {
            toast.error("Failed to fetch contents");
        }
    };

    const handleFileChange = (event) => {
        setFormData({ ...formData, images: event.target.files });
    };

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
            return;
        }
        setValidated(true);

        try {
            const contentData = new FormData();
            if (formData.text) {
                contentData.append("text", formData.text);
            }
            if (formData.images) {
                for (let i = 0; i < formData.images.length; i++) {
                    contentData.append("images", formData.images[i]);
                }
            }
            if (formData.category) {
                contentData.append("category", formData.category);
            }

            const method = formData.id ? "PUT" : "POST";
            const url = formData.id
                ? `http://localhost:8080/api/content/${formData.id}`
                : "http://localhost:8080/api/content";

            const response = await fetch(url, {
                method: method,
                body: contentData,
            });

            if (response.ok) {
                toast.success(`Content ${formData.id ? "updated" : "uploaded"} successfully`);
                setShowModal(false);
                setFormData({
                    id: null,
                    text: "",
                    images: null,
                    category: "attractions"
                });
                fetchContents();
            } else {
                const errorData = await response.json();
                toast.error(errorData.message);
            }
        } catch (error) {
            toast.error("An error occurred while uploading the content");
        }
    };

    const handleEdit = (content) => {
        setFormData({
            id: content._id,
            text: content.text,
            images: null,
            category: content.category
        });
        setShowModal(true);
    };

    const handleDelete = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/content/${contentIdToDelete}`, {
                method: "DELETE",
            });

            if (response.ok) {
                toast.success("Content deleted successfully");
                fetchContents();
            } else {
                const errorData = await response.json();
                toast.error(errorData.message);
            }
        } catch (error) {
            toast.error("An error occurred while deleting the content");
        } finally {
            handleCloseDeleteModal();
        }
    };

    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [contentIdToDelete, setContentIdToDelete] = useState(null);

    const handleShowDeleteModal = (id) => {
        setContentIdToDelete(id);
        setShowDeleteModal(true);
    };

    const handleCloseDeleteModal = () => {
        setShowDeleteModal(false);
        setContentIdToDelete(null);
    };

    return (
        <div className="container p-5">
            <div className="d-flex justify-content-end mb-3">
                <button className="custom-btn" onClick={() => {
                    setFormData({ id: null, text: "", images: null, category: "attractions" });
                    setShowModal(true);
                }}><i className="fas fa-plus"></i> Upload Content
                </button>
            </div>

            <Modal className="center-modal" show={showModal} onHide={() => setShowModal(false)}>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formData.id ? "Update" : "Upload"} Content</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="formText">
                            <Form.Label>Text</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter text"
                                name="text"
                                value={formData.text}
                                onChange={handleChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please provide some text.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formImages">
                            <Form.Label>Images</Form.Label>
                            <Form.Control
                                type="file"
                                name="images"
                                multiple
                                onChange={handleFileChange}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please upload images.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className='custom-btn' onClick={() => setShowModal(false)}>
                            Cancel
                        </button>
                        <Button type="submit" className="upload-button ml-5">
                            {formData.id ? "Update" : "Upload"}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            <Modal className="center-modal" show={showDeleteModal} onHide={handleCloseDeleteModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm Deletion</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Are you sure you want to delete this content?
                </Modal.Body>
                <Modal.Footer>
                    <button className='custom-btn' onClick={() => setShowDeleteModal(false)}>
                        Cancel
                    </button>
                    <Button className="ml-5" variant="danger" onClick={handleDelete}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>

            <div className="column mt-5">
                {contents.map(content => (
                    <div className="mb-4 " key={content._id}>
                        <Card className='d-flex flex-row flex-wrap border-0'>
                            {content.images && content.images.length > 0 && (
                                <Card.Img className='image-size border-radius' variant="top" src={content.images[0]} />
                            )}
                            <Card.Body className='col-sm-6'>
                                <Card.Text>{content.text}</Card.Text>
                            </Card.Body>
                        </Card>
                        <div className='w-100 text-end mt-3 mb-5'>
                            <button className="custom-btn bg-white border-0 text-black" onClick={() => handleEdit(content)}>
                                <i className="fas fa-edit"></i> Edit
                            </button>
                            <button className='custom-btn ml-5' onClick={() => handleShowDeleteModal(content._id)}>
                                <i className="fas fa-trash-alt"></i> Delete
                            </button>
                        </div>
                    </div>
                ))}
            </div>

        </div>
    );
};

export default Attraction;
