import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { toast } from "react-toastify";
import { Container, Table, Alert, Button } from 'react-bootstrap';
import { FaCheck, FaTimes } from 'react-icons/fa';
import 'react-toastify/dist/ReactToastify.css';

const ReservationTable = () => {
    const [reservations, setReservations] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetchReservations();
    }, []);

    const fetchReservations = () => {
        axios.get('http://localhost:8080/api/userTicket/')
            .then(response => {
                setReservations(response.data);
            })
            .catch(error => {
                setError(error);
                console.error('Error fetching reservations:', error);
            });
    };

    const handleConfirm = (id) => {
        axios.put(`http://localhost:8080/api/userTicket/${id}`, { confirmed: true })
            .then(response => {
                console.log('Transaction confirmed:', response.data);
                setReservations(prevReservations =>
                    prevReservations.map(reservation =>
                        reservation._id === id ? { ...reservation, confirmed: true } : reservation
                    )
                );
                toast.success("Confirmed Successfully");
            })
            .catch(error => {
                setError(error.response ? error.response.data.message : error.message);
                console.error('Error confirming transaction:', error);
            });
    };

    const handleDelete = (id) => {
        axios.delete(`http://localhost:8080/api/userTicket/${id}`)
            .then(response => {
                console.log('Transaction deleted:', response.data);
                setReservations(prevReservations => prevReservations.filter(reservation => reservation._id !== id));
                toast.error("Deleted successfully");
            })
            .catch(error => {
                setError(error.response ? error.response.data.message : error.message);
                console.error('Error deleting transaction:', error);
            });
    };

    return (
        <Container className="mt-5">
            <h2>Reservation Table</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Table bordered responsive>
                <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th>Reservation Holder</th>
                        <th>CID</th>
                        <th>Tickets Details</th>
                        <th>Amount</th>
                        <th>Contact Info</th>
                        <th>Journal Number</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {reservations.map((reservation, index) => (
                        <tr key={reservation._id || index}>
                            <td>{index + 1}</td>
                            <td>{reservation.name}</td>
                            <td>{reservation.cid}</td>
                            <td className='text-center'>
                                {reservation.numTickets_1 > 0 && (
                                    <div>Adult: {reservation.numTickets_1}</div>
                                )}
                                {reservation.numTickets_2 > 0 && (
                                    <div>Child: {reservation.numTickets_2}</div>
                                )}
                            </td>
                            <td>{<div>Nu:/ {reservation.totalAmount}</div>}</td>
                            <td>
                                {reservation.email && (
                                    <div>Email: {reservation.email}</div>
                                )}
                                {reservation.phoneNumber && (
                                    <div>Phone Number: {reservation.phoneNumber}</div>
                                )}
                            </td>
                            <td>{reservation.journalNumber}</td>
                            <td>
                                {reservation.confirmed ? (
                                    <Button variant="success" disabled>
                                        <FaCheck className="me-1" />
                                        Checked
                                    </Button>
                                ) : (
                                    <Button
                                        variant="warning"
                                        onClick={() => handleConfirm(reservation._id)}
                                    >
                                        <FaTimes className="me-1" />
                                        Pending
                                    </Button>
                                )}
                            </td>
                            <td>
                                <Button variant="danger" className="ms-2" onClick={() => handleDelete(reservation._id)}>
                                    Delete
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    );
};

export default ReservationTable;
