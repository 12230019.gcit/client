import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function StaticExample({ show, handleClose }) {
    const [notifications, setNotifications] = useState([
        { id: 1, message: 'Your ticket is on pending', date: 'March 19, 2024 at 03:15 AM' },
    ]);

    const [showMore, setShowMore] = useState(false);

    const handleShowMore = () => {
        setShowMore(true);
        // Simulate fetching more notifications
        const moreNotifications = [
            { id: 2, message: 'Your request has been approved', date: 'March 19, 2024 at 03:15 AM' },
            { id: 3, message: 'Your subscription is expiring soon', date: 'March 19, 2024 at 03:15 AM' },
        ];
        setNotifications([...notifications, ...moreNotifications]);
    };

    return (
        <Modal show={show} onHide={handleClose} centered>
            <Modal.Header closeButton>
                <Modal.Title>Notification</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {notifications.map(notification => (
                    <div key={notification.id} style={{ marginBottom: '20px' }}>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <p style={{ marginBottom: '5px' }}>{notification.message}</p>
                            <span style={{ color: 'gray' }}>{notification.date}</span>
                        </div>
                    </div>
                ))}
            </Modal.Body>

            <Modal.Footer>
                {!showMore && (
                    <Button variant="link" onClick={handleShowMore}>
                        View all Notifications
                    </Button>
                )}
            </Modal.Footer>
        </Modal>
    );
}

export default StaticExample;
