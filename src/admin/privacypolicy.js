import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// import '../../css/admin/style.css'

const PrivacyPolicy = () => {
    return (
        <div className='pp_container d-flex row p-5'>
            <h2 mb-5>Privacy Policy</h2>
            <section>
                <h4>Introduction</h4>
                <p>
                    Welcome to our water park reservation system! This privacy policy outlines how we collect, use, share, and protect your personal information. By using our website, you agree to the terms described below.
                </p>
            </section>

            <section>
                <h4>Information We Collect</h4>
                <ol>
                    <li>User Registration Data: When you sign up or log in, we collect information such as your name, email address, and password.</li>
                    <li>Reservation Details: We collect data related to your water park reservations, including dates, ticket types, and group size.</li>
                    <li>Payment Information: If you make a reservation, we collect payment details (credit card information, billing address) through secure channels.</li>
                    <li>Communication Data: We may collect information from your interactions with our customer support or feedback forms.</li>
                    <li>Automatically Collected Data: Our website uses cookies and similar technologies to collect data on your browsing behavior, IP address, device type, and location.</li>
                </ol>
            </section>

            <section>
                <h4>How We Use Your Information</h4>
                <ol>
                    <li>Reservation Management: We use your data to process reservations, allocate tickets, and manage capacity.</li>
                    <li>Communication: We may send you reservation confirmations, updates, and promotional offers via email.</li>
                    <li>Improvement and Analytics: We analyze user behavior to enhance our services and improve the website experience.</li>
                </ol>
            </section>

            <section>
                <h4>Data Sharing</h4>
                <ol>
                    <li>Third-Party Services: We may share your information with third-party service providers (e.g., payment gateways, analytics tools) to facilitate reservations and improve our services.</li>
                    <li>Legal Compliance: We may disclose your data if required by law or to protect our rights, safety, or property.</li>
                </ol>
            </section>

            <section>
                <h4>Your Rights</h4>
                <ol>
                    <li>Access and Correction: You can access and update your personal information by logging into your account.</li>
                    <li>Opt-Out: You can unsubscribe from marketing emails or delete your account at any time.</li>
                </ol>
            </section>

            <section>
                <h4>Security Measures</h4>
                <p>We take reasonable steps to protect your data from unauthorized access, loss, or misuse.</p>
            </section>

            <section>
                <h4>Contact Us</h4>
                <p>If you have any questions or concerns about our privacy practices, please contact us.</p>
            </section>
        </div>
    );
};

export default PrivacyPolicy;

