import React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const Sitemap = () => {
    return (
        <div className="container" style={{ marginTop: "100px" }}>
            <div><strong style={{ fontSize: "30px" }}>Yodi Site Map</strong>
                <div className=" blue-line-3"></div>
            </div>
            <div className="row mt-5">
                <div className="col-3">
                    <ul className="row">
                        <Link to="/admin/home" style={{ textDecoration: "none", color: "black" }}>Home</Link>
                        <Link to="/admin/transactions" style={{ textDecoration: "none", color: "black" }}>Transaction</Link>
                        <Link to="/admin/aboutus" style={{ textDecoration: "none", color: "black" }}>About</Link>
                        <Link to="/admin/attraction" style={{ textDecoration: "none", color: "black" }}>Attraction</Link>
                        <Link to="/admin/tickets" style={{ textDecoration: "none", color: "black" }}>Ticket</Link>
                        <Link to="/admin/privacypolicy" style={{ textDecoration: "none", color: "black" }}>Privacy policy</Link>
                        <Link to="/admin/rulesandregulations" style={{ textDecoration: "none", color: "black" }}>Rules and Regulations</Link>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Sitemap;
