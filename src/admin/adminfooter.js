import React from "react";
import '../css/admin/footer.css';
import { NavLink } from "react-router-dom";
import { FaFacebook, FaTwitter, FaInstagram, FaYoutube, FaTiktok } from 'react-icons/fa'; // Import social media icons

const Footer = () => {
  return (
    <div>
      <footer className="footer">
        <div className="container-fluid">
          <div className="d-flex row">
            <div className="footer1 col-12 d-flex justify-content-center align-items-center">
              <div className="d-flex flex-column justify-content-center align-items-center gap-4">
                <NavLink to="/admin/home">              <img
                  src="../../.././Assets/logowhite.png"
                  alt="logo"
                  style={{ width: "10em" }}
                />
                </NavLink>
                <h3 className="footer-heading" style={{ color: "white", fontWeight: "bold" }}>Aqua Park</h3>
                <div className="social-icons mt-3">
                  <FaFacebook className="social-icon" />
                  <FaTwitter className="social-icon" />
                  <FaYoutube className="social-icon" />
                  <FaTiktok className="social-icon" />
                  <FaInstagram className="social-icon" />
                </div>
                <ul
                  className="links d-flex flex-row flex-wrap"
                  style={{ listStyle: "none" }}
                >
                  <li className="links-items">
                    <NavLink to="/admin/rulesandregulations" className="links-items px-4 text-white">
                      Rules and Regulations
                    </NavLink>
                  </li>
                  <li className="line">|</li>
                  <li className="links-items">
                    <NavLink to="/admin/privacypolicy" className="links-items px-4 text-white">
                      Privacy Policy
                    </NavLink>
                  </li>
                  <li className="line">|</li>

                  <li className="links-items">
                    <NavLink to="/admin/sitemap" className="links-items px-4 text-white">
                      Sitemap
                    </NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <div className="row">
        <div className="col-12 text-center mt-2">
          <p className=" footer__copyright">
            © {new Date().getFullYear()} All rights reserved
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
