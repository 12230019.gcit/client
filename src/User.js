import React from 'react';
import { Routes, Route} from 'react-router-dom';

import HeroBanner from './components/herobanner/herobanner';
import Navbar from "./components/common/header/Nav";
import Footer from "./components/common/footer/footer";
import Home from '../src/pages/Home';
import AboutUs from '../src/pages/about';
import BookTicket from '../src/pages/Ticket';
import PrivacyPolicy from '../src/pages/privacypolicy';
import Help from '../src/pages/help';
import Attraction from '../src/pages/attractions';
import Rules from '../src/pages/rulesAndRegulation';
import Sitemap from '../src/pages/siteMap';

function UserLayout({ onLogout }) {


    return (
        <div>
            <Navbar onLogout={onLogout} />
            <div className="main-content">
                <Routes>
                    <Route path="/herobanner" element={<HeroBanner />} />
                    <Route path="/home" element={<Home />} />
                    <Route path="/aboutus" element={<AboutUs />} />
                    <Route path="/tickets" element={<BookTicket />} />
                    <Route path="/privacypolicy" element={<PrivacyPolicy />} />
                    <Route path="/help" element={<Help />} />
                    <Route path="/attraction" element={<Attraction />} />
                    <Route path="/rules" element={<Rules />} />
                    <Route path="/sitemap" element={<Sitemap />} />
                </Routes>
            </div>
            <Footer />
        </div>
    );
}

export default UserLayout;
