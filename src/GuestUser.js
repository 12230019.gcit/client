import React from 'react';
import { Routes, Route } from 'react-router-dom';

import HeroBanner from './components/herobanner/herobanner';
import Header from "./components/common/guestheader/guestheader";
import Footer from "./components/common/footer/footer";
import Home from './pages/Home';
import AboutUs from './pages/about';
import PrivacyPolicy from './pages/privacypolicy';
import Help from './pages/help';
import Attraction from './pages/attractions';
import Rules from './pages/rulesAndRegulation';
import Sitemap from './pages/siteMap';
import Signup from './pages/Signup';
import Login from './pages/login';

function GuestUserLayout({ onLogout }) {
    return (
        <div>
            <Header onLogout={onLogout} />
            <div className="main-content">
                <Routes>
                    <Route path="/herobanner" element={<HeroBanner />} />
                    <Route path="/" element={<Home />} />
                    <Route path="/signup" element={<Signup />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/aboutus" element={<AboutUs />} />
                    <Route path="/privacypolicy" element={<PrivacyPolicy />} />
                    <Route path="/help" element={<Help />} />
                    <Route path="/attraction" element={<Attraction />} />
                    <Route path="/rules" element={<Rules />} />
                    <Route path="/sitemap" element={<Sitemap />} />
                </Routes>
            </div>
            <Footer />
        </div>
    );
}

export default GuestUserLayout;
